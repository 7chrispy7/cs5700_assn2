

config = dict(
    UDP_IP='127.0.0.1',
    drone_PORT=8889,
    state_PORT=9000,
    timeout=1.0,
    max_retries=3
)