

class State:
    state = None

    def __init__(self):
        self.state = {
        'pitch': 0.0,
        'roll': 0.0,
        'yaw': 0.0,
        'vgx': 0.0,
        'vgy': 0.0,
        'vgz': 0.0,
        'templ': 50.0,
        'temph': 75.0,
        'tof': 0.0,
        'h': 0.0,
        'bat': 100.0,
        'baro': 0.0,
        'time': 0.0,
        'agx': 0.0,
        'agy': 0.0,
        'agz': 0.0
    }

    def getState(self):
        return 'pitch:' + str(self.state['pitch']) + ';roll:' + str(self.state['roll']) + ';yaw:' + str(self.state['yaw']) + \
               ';vgx:' + str(self.state['vgx']) + ';vgy:' + str(self.state['vgy']) + ';vgz:' + str(self.state['vgz']) + \
               ';templ:' + str(self.state['templ']) + ';temph:' + str(self.state['temph']) + ';tof:' + str(self.state['tof']) + \
               ';h:' + str(self.state['h']) + ';bat:' + str(self.state['bat']) + ';baro:' + str(self.state['baro']) + \
               ';time:' + str(self.state['time']) + ';agx:' + str(self.state['agx']) + ';agy:' + str(self.state['agy']) + \
               ';agz:' + str(self.state['agz'])

    def updateState(self, newState):
        #newState should be in string form as given by the getState function.
        newState = newState.split(';')
        for string in newState:
            string = string.split(':')
            self.state[string[0]] = float(string[1])