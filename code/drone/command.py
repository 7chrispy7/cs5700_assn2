import time


class Command:
    command = None

    def execute(self, drone):
        if not self.shouldAbort(drone):
            self.runCommand(drone)
            return True
        else:
            return False

    def shouldAbort(self, drone):
        if drone.state.state['bat'] < 5:
            c = DisplayInfo('Critical battery level! Mission aborted.')
            c.runCommand(drone)
            return True
        if drone.state.state['temph'] > 120:
            c = DisplayInfo('Critical high temperature! Mission aborted')
            c.runCommand(drone)
            return True
        return False

    def runCommand(self, drone):
        pass


class Ping(Command):
    command = "command"

    def runCommand(self,drone):
        drone.sendmessage(self.command)


class Launch(Command):
    command = "takeoff"

    def runCommand(self, drone):
        drone.sendmessage(self.command)
        drone.reset_location()
        drone.add_location_up_down(20)


class Land(Command):
    command = "land"

    def runCommand(self, drone):
        drone.sendmessage(self.command)
        drone.reset_location()


class Fly(Command):
    command = None
    direction = None
    distance = None

    def __init__(self, direction, distance):
        directions = {
            'u': "up",
            'd': "down",
            'l': "left",
            'r': "right",
            'f': "forward",
            'b': "back"
        }
        self.direction = direction
        self.distance = distance
        self.command = directions.get(direction) + ' ' + str(distance)

    def runCommand(self, drone):
        drone.sendmessage(self.command)
        if self.direction == 'u':
            drone.add_location_up_down(self.distance)
        elif self.direction == 'd':
            drone.add_location_up_down(-self.distance)
        elif self.direction == 'r':
            drone.add_location_left_right(self.distance)
        elif self.direction == 'l':
            drone.add_location_left_right(-self.distance)
        elif self.direction == 'f':
            drone.add_location_forward_back(self.distance)
        elif self.direction == 'b':
            drone.add_location_forward_back(-self.distance)


class Flip(Command):
    command = None
    direction = None
    directions = {
            'l': "left",
            'r': "right",
            'f': "forward",
            'b': "back"
        }

    def __init__(self, direction):
        self.command = 'flip ' + str(direction)
        self.direction = direction

    def runCommand(self, drone):
        if drone.state.state['bat'] <= 20:
            self.command = self.command.split(' ')
            self.command = self.directions.get(self.command[1]) + ' 20'
        drone.sendmessage(self.command)
        if self.direction == 'r':
            drone.add_location_left_right(20)
        elif self.direction == 'l':
            drone.add_location_left_right(-20)
        elif self.direction == 'f':
            drone.add_location_forward_back(20)
        elif self.direction == 'b':
            drone.add_location_forward_back(-20)


class Rotate(Command):
    command = None
    direction = None

    def __init__(self,direction,degrees):
        self.direction = direction
        directions = {
            'l': "ccw",
            'r': "cw"
        }
        self.command = directions.get(direction) + ' ' + str(degrees)

    def runCommand(self, drone):
        drone.sendmessage(self.command)
        drone.rotate(self.direction)


class DisplayInfo(Command):
    command = None

    def __init__(self, message):
        self.command = message

    def runCommand(self, drone):
        print(self.command)


class Pause(Command):
    command = 0

    def __init__(self, num_secs):
        command = num_secs

    def runCommand(self, drone):
        time.sleep(self.command)


class getInput(Command):
    command = ''

    def __init__(self, prompt):
        command = prompt

    def runCommand(self, drone):
        input(str(self.command))
