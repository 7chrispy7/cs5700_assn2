from unittest import TestCase
from state import State


class TestState(TestCase):
    def test_get_state(self):
        s = State()
        assert s.getState() == 'pitch:0.0;roll:0.0;yaw:0.0;vgx:0.0;vgy:0.0;vgz:0.0;templ:50.0;temph:75.0;tof:0.0;h:0.0;bat:100.0;baro:0.0;time:0.0;agx:0.0;agy:0.0;agz:0.0'

    def test_update_state(self):
        s = State()
        s.state['pitch'] = 2.0
        s_1 = State()
        assert s_1.getState() == 'pitch:0.0;roll:0.0;yaw:0.0;vgx:0.0;vgy:0.0;vgz:0.0;templ:50.0;temph:75.0;tof:0.0;h:0.0;bat:100.0;baro:0.0;time:0.0;agx:0.0;agy:0.0;agz:0.0'
        s_1.updateState(s.getState())
        print(s_1.getState())
        assert s_1.getState() == 'pitch:2.0;roll:0.0;yaw:0.0;vgx:0.0;vgy:0.0;vgz:0.0;templ:50.0;temph:75.0;tof:0.0;h:0.0;bat:100.0;baro:0.0;time:0.0;agx:0.0;agy:0.0;agz:0.0'

