from Queue import Queue
from drone import Drone
from command import *

class Mission:
    commands = Queue()
    aborted = False

    def run(self, drone):
        while not self.commands.empty() and not self.aborted:
            command = self.commands.get()
            if not command.execute(drone):
                self.abort(drone)

    def abort(self, drone):
        self.aborted = True
        drone.land()


class Mission1(Mission):
    def __init__(self):
        self.commands.put(Ping())
        self.commands.put(Launch())
        self.commands.put(Fly('l',40))
        self.commands.put(Fly('r',40))
        self.commands.put(Fly('f',40))
        self.commands.put(Fly('b',40))
        self.commands.put(Fly('u',40))
        self.commands.put(Fly('d',40))
        self.commands.put(Land())


class Mission2(Mission):
    def __init__(self):
        self.commands.put(Ping())
        self.commands.put(Launch())
        self.commands.put(Flip('l'))
        self.commands.put(Flip('r'))
        self.commands.put(Flip('f'))
        self.commands.put(Flip('b'))
        self.commands.put(Land())


class Mission3(Mission):
    def __init__(self):
        self.commands.put(Ping())
        self.commands.put(Launch())
        self.commands.put(Rotate('r', 90))
        self.commands.put(Fly('f', 400))
        self.commands.put(Rotate('l', 90))
        self.commands.put(Fly('f', 400))
        self.commands.put(Rotate('l', 90))
        self.commands.put(Fly('f', 800))
        self.commands.put(Rotate('l', 90))
        self.commands.put(Fly('f', 800))
        self.commands.put(Rotate('l', 90))
        self.commands.put(Fly('f', 800))
        self.commands.put(Rotate('l', 90))
        self.commands.put(Fly('f', 400))
        self.commands.put(Rotate('l', 90))
        self.commands.put(Fly('f', 400))
        self.commands.put(Rotate('r', 90))
        self.commands.put(Land())


class MissionAbortTemp(Mission):
    def __init__(self):
        self.commands.put(Rotate('r', 90))
        self.commands.put(Rotate('r', 90))
        self.commands.put(Rotate('r', 90))
        self.commands.put(Rotate('r', 90))
        self.commands.put(Rotate('r', 90))
        self.commands.put(Rotate('r', 90))
        self.commands.put(Rotate('r', 90))
        self.commands.put(Rotate('r', 90))
        self.commands.put(Rotate('r', 90))
        self.commands.put(Rotate('r', 90))
        self.commands.put(Rotate('r', 90))
        self.commands.put(Rotate('r', 90))


class MissionAbortBat(Mission):
    def __init__(self):
        self.commands.put(Flip('l'))
        self.commands.put(Flip('r'))
        self.commands.put(Flip('l'))
        self.commands.put(Flip('r'))
        self.commands.put(Flip('l'))
        self.commands.put(Flip('r'))
        self.commands.put(Flip('l'))
        self.commands.put(Flip('r'))
        self.commands.put(Flip('l'))
        self.commands.put(Flip('r'))
        self.commands.put(Flip('l'))
        self.commands.put(Flip('r'))
        self.commands.put(Flip('l'))
        self.commands.put(Flip('r'))
        self.commands.put(Flip('l'))
        self.commands.put(Flip('r'))
        self.commands.put(Flip('l'))
        self.commands.put(Flip('r'))
        self.commands.put(Flip('l'))
        self.commands.put(Flip('r'))
        self.commands.put(Flip('l'))
        self.commands.put(Flip('r'))
        self.commands.put(Flip('l'))
        self.commands.put(Flip('r'))
        self.commands.put(Flip('l'))
        self.commands.put(Flip('r'))
        self.commands.put(Flip('l'))
        self.commands.put(Flip('r'))
        self.commands.put(Flip('l'))
        self.commands.put(Flip('r'))
        self.commands.put(Flip('l'))
        self.commands.put(Flip('r'))
        self.commands.put(Flip('l'))
        self.commands.put(Flip('r'))
