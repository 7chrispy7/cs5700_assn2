from drone import Drone
from multiprocessing import Process
from mission import *

import os, sys, inspect
sys.path.insert(1, os.path.join(sys.path[0], '..'))
import config


class UserInterface:
    drone = None

    def initDrone(self):
        print("Welcome to the drone controller!")
        print("Please note default values will use the simulator rather than a drone.\n")
        custom = raw_input('Would you like to define custom configuration values? [y/n]\n')
        if custom == 'n':
            UDP_IP = config.config['UDP_IP']
            UDP_PORT = config.config['drone_PORT']
            state_PORT = config.config['state_PORT']
            time_out = config.config['timeout']
            max_retries = config.config['max_retries']
            self.drone = Drone(UDP_IP,UDP_PORT,state_PORT,time_out,max_retries)
        else:
            UDP_IP = raw_input("Please enter an IP: ")
            UDP_PORT = raw_input("Please enter a port for outbound messages: ")
            state_PORT = raw_input("Please enter a port for inbound messages: ")
            time_out = raw_input("Please enter a communication timeout value: ")
            max_retries = raw_input("Please give a maximum number of retries for messages: ")
            self.drone = Drone(UDP_IP,UDP_PORT,state_PORT,time_out,max_retries)

    def runMission(self, mission):
        toRun = None
        if mission == '1':
            toRun = Mission1()
        elif mission == '2':
            toRun = Mission2()
        elif mission == '3':
            toRun = Mission3()
        elif mission == '4':
            toRun = MissionAbortBat()
        elif mission == '5':
            toRun = MissionAbortTemp()
        else:
            print('That is not a valid mission.  Please choose a mission [1-3].')
            return None
        toRun.run(self.drone)
    def mainLoop(self):
        isDone = False
        while not isDone:
            print('Please select a mission [1-3].\nOr type [q] to quit.')
            mission = raw_input()
            if mission != 'q':
                self.runMission(mission)
            else:
                isDone = True
