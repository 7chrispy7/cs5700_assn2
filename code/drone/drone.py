import time
from state import State
from communicator import Communicator
from multiprocessing import Process, Manager
from ctypes import c_char_p


class Drone:
    communicator = None
    stateReceiver = None
    UDP_IP = None
    UDP_PORT = None
    state = State()
    location = [0.0, 0.0, 0.0]      # x, y, z
    orientation = 0                 # [0-3] increasing clockwise
    stateProcess = None

    def __init__(self, UDP_IP, UDP_PORT, state_PORT, time_out, max_retries):
        self.UDP_IP = UDP_IP
        self.UDP_PORT = UDP_PORT
        self.state_PORT = state_PORT
        self.time_out = time_out
        self.max_retries = max_retries
        #self.stateProcess = Process(target=self.listenForState)
        #self.stateProcess.start()

    def listenForState(self):
        state = None
        self.stateReceiver = Communicator(self.UDP_IP, self.state_PORT, self.time_out)
        while True:
            state, address = self.stateReceiver.recvMessage()
            if state is not None:
                self.state.updateState(state)

    def sendmessage(self, message):
        if self.communicator == None:
            self.communicator = Communicator(self.UDP_IP, self.UDP_PORT, self.time_out, bind=False)
            self.stateReceiver = Communicator(self.UDP_IP, self.state_PORT, self.time_out)
        self.communicator.sendMessage(message)
        retries = self.max_retries
        time.sleep(1)
        recvMessage, addr = self.communicator.recvMessage()
        while recvMessage != 'ok' and retries > 0:
            recvMessage, addr = self.communicator.recvMessage()
            time.sleep(1)
            retries -= 1
        if recvMessage != 'ok':
            print('Ok message not received')
            print('Message: ' + message)
        else:
            time.sleep(2)
        state, address = self.stateReceiver.recvMessage()
        if state is not None:
            self.state.updateState(state)

    def ping(self):
        self.sendmessage('command')

    def launch(self):
        self.sendmessage('takeoff')

    def land(self):
        self.sendmessage('land')

    def fly(self, direction, distance):
        directions = {
            'u': "up",
            'd': "down",
            'l': "left",
            'r': "right",
            'f': "forward",
            'b': "back"
        }
        self.sendmessage(directions.get(direction) + ' ' + str(distance))

    def flip(self, direction):
        self.sendmessage('flip ' + direction)

    def rotate(self, direction, degrees):
        directions = {
            'l': "ccw",
            'r': "cw"
        }
        self.sendmessage(directions.get(direction) + ' ' + str(degrees))

    def add_location_left_right(self, x):
        if self.orientation == 0:
            self.location[0] += x
        elif self.orientation == 1:
            self.location[1] -= x
        elif self.orientation == 2:
            self.location[0] -= x
        elif self.orientation == 3:
            self.location[1] += x

    def add_location_forward_back(self, x):
        if self.orientation == 0:
            self.location[1] += x
        elif self.orientation == 1:
            self.location[0] += x
        elif self.orientation == 2:
            self.location[1] -= x
        elif self.orientation == 3:
            self.location[0] -= x

    def add_location_up_down(self, x):
        self.location[0] += x

    def reset_location(self):
        self.location = [0.0, 0.0, 0.0]
        self.orientation = 0

    def rotate(self, direction):
        if direction == 'l':
            self.orientation -= 1
        elif direction == 'r':
            self.orientation += 1
        if direction > 3:
            self.orientation = 0
        if direction < 0:
            self.orientation = 3
