from communicator import Communicator
from threading import Thread
from multiprocessing import Process
import time
import state


class Simulator:
    msgComm = None
    stateComm = None
    state = None
    flightStartTime = None

    def __init__(self):
        print('Welcome to the drone simulator. Listening on port 8889.')
        print('State on port 9000.')
        self.state = state.State()

    def doCommand(self, command):
        print(command)
        if self.stateComm == None:
            self.stateComm = Communicator('127.0.0.1', 9000, bind=False)
        if self.flightStartTime is not None:
            self.state.state['time'] = time.time() - self.flightStartTime
        command = command.split(' ')
        self.state.state['bat'] -= 1
        self.state.state['temph'] += 1
        if command[0] == 'takeoff':
            self.state.state['h'] += 20
            self.flightStartTime = time.time()
        elif command[0] == 'land':
            #reset state
            self.state.state = {
                'pitch': 0.0,
                'roll': 0.0,
                'yaw': 0.0,
                'vgx': 0.0,
                'vgy': 0.0,
                'vgz': 0.0,
                'templ': 50.0,
                'temph': 75.0,
                'tof': 0.0,
                'h': 0.0,
                'bat': 100.0,
                'baro': 0.0,
                'time': 0.0,
                'agx': 0.0,
                'agy': 0.0,
                'agz': 0.0
            }
            self.flightStartTime = None
        elif command[0] == 'up':
            self.state.state['h'] += float(command[1])
        elif command[0] == 'down':
            self.state.state['h'] -= float(command[1])
            if self.state.state['h'] < 20:
                self.state.state['h'] = 20
        elif command[0] == 'flip':
            self.state.state['bat'] -= 4
        elif command[0] == 'cw' or command[0] == 'ccw':
            self.state.state['temph'] += 5
        self.stateComm.sendMessage(self.state.getState())


    def listen(self):
        self.msgComm = Communicator('127.0.0.1', 8889)
        while True:
            message, addr = self.msgComm.recvMessage()
            if message is not None:
                self.doCommand(message)
                self.msgComm.sendMessage('ok', addr)

    def pingState(self):
        self.stateComm = Communicator('127.0.0.1', 9000, bind=False)
        while True:
            #self.stateComm.sendMessage(self.state.getState())
            time.sleep(0.1)
            pass


if __name__ == '__main__':
    sim = Simulator()
    listenThread = Process(target=sim.listen)
    listenThread.start()
    stateThread = Process(target=sim.pingState)
    stateThread.start()
    listenThread.join()
    stateThread.join()
