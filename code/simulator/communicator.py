import socket
import time
import pickle

class Communicator:
    __socket = None
    __UDP_IP = None
    __UDP_PORT = None

    def __init__(self, UDP_IP, UDP_PORT, time_out=1.0, bind=True):
        self.__UDP_IP = UDP_IP
        self.__UDP_PORT = int(UDP_PORT)
        self.__socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        if bind is True:
            try:
                self.__socket.bind((self.__UDP_IP,self.__UDP_PORT))
            except:
                print("socket failed to bind to " + str(self.__UDP_IP) + " on " + str(self.__UDP_PORT))
        self.__socket.settimeout(time_out)

    def sendMessage(self,message,address=None):
        message=message.encode('UTF-8')
        if address != None:
            self.__socket.sendto(message,address)
        else:
            self.__socket.sendto(message,(self.__UDP_IP,self.__UDP_PORT))

    def recvMessage(self):
        message=None
        address=None
        try:
            (message,address)=self.__socket.recvfrom(256)
            message=message.decode('UTF-8')
        except:
            pass
        return message,address

    def sendObject(self, object):
        message=pickle.dumps(object)
        self.__socket.sendto(message,(self.__UDP_IP,self.__UDP_PORT))

    def recvObject(self):
        object = None
        address = None
        try:
            (message,address)=self.__socket.recvfrom(256)
            object = pickle.loads(message)
        except:
            pass
        return object, address
